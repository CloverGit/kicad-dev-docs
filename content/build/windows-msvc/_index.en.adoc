---
title: Windows (Visual Studio)
weight: 14
summary: Guide on building KiCad using Microsoft Visual Studio and vcpkg
tags: ["windows"]
---
:toc:

== Building using Visual Studio (2019)

=== Environment Setup

==== Visual Studio
You must first install https://visualstudio.microsoft.com/vs/[Visual Studio] with the **Desktop development with {cpp}** feature set installed.
Additionally, you'll need to make sure the optional component https://docs.microsoft.com/en-us/cpp/build/cmake-projects-in-visual-studio?view=msvc-160#installation[{cpp} CMake tools for Windows] is installed.

==== vcpkg (kicad fork required)

KiCad maintains a fork of vcpkg to provide us with a wxPython port.
It also allows us to lock our dependencies as vcpkg updates fairly frequently.

**If you are new to vcpkg** you must, pick a spot on your system to put it.
Then run these three commands

[source,powershell]
```
git clone https://gitlab.com/kicad/packaging/vcpkg
.\vcpkg\bootstrap-vcpkg.bat
.\vcpkg\vcpkg integrate install
```

which will give you a vcpkg install ready to use with the next steps.

=== KiCad Specific Setup

vcpkg defaults to x86-windows even on 64-bit machines,
it is advised for ease of use you set a **USER** or **SYSTEM** environment variable
with the name **VCPKG_DEFAULT_TRIPLET** and value **x64-windows**

KiCad still supports 32-bit builds for now but may not in the future, thus 64-bit is preferred.

==== 1. Install dependencies
Most of KiCad's dependencies can be installed using `vcpkg` as described below:

[source,powershell]
```
.\vcpkg install boost cairo curl glew gettext glm icu libxslt ngspice opencascade opengl openssl python3 wxpython wxwidgets zlib harfbuzz
```

If you did not set the **VCPKG_DEFAULT_TRIPLET** environment variable, you will have to append
:x64-windows to end of each packages name as below:

[source,powershell]
```
.\vcpkg install boost:x64-windows cairo:x64-windows curl:x64-windows glew:x64-windows gettext:x64-windows glm:x64-windows `
icu:x64-windows libxslt:x64-windows ngspice:x64-windows opencascade:x64-windows opengl:x64-windows `
python3:x64-windows wxpython:x64-windows wxwidgets:x64-windows zlib:x64-windows harfbuzz:x64-windows
```

NOTE: If the vcpkg install fails on wxpython, you did not clone and checkout the `kicad` branch from https://gitlab.com/kicad/packaging/vcpkg for vcpkg.

Additionally, SWIG must be installed manually.  Obtain the latest `swigwin` package from
https://sourceforge.net/projects/swig/files/swigwin/ (at the time of this writing, the latest is
`swigwin-4.0.2`) and extract the zip file to a known location.

==== 2. CMakeSettings.json
Contained in the build root is a `CMakeSettings.json.sample`, copy and rename this file to `CMakeSettings.json`
Edit `CMakeSettings.json` update the VcPkgDir environment variable up top to match the location of your vcpkg clone.

[source,json]
----
{ "VcPkgDir": "D:/vcpkg/" },
----

==== 3. "Open Folder" in Visual Studio
* Launch Visual Studio (only after completing the above steps).
* When the initial wizard launches, select to **Open a local folder**. 
This is the correct way to make Visual Studio directly handle *CMake* projects.
* Select the build root folder.
* Wait until the "CMake in Visual Studio" tab appears and the CMake process hits an error, which we'll fix in the next step.

==== 4. Configure paths to Python and SWIG

At present, you must add CMake arguments to the location of Python installed by `vcpkg` and SWIG
that you downloaded in a previous step.  Choose the Configuration you want to use (e.g. x64-Debug) in the toolbar. Then, open the CMake Settings editor (Project menu > CMake
Settings), and add the following items to the CMake Command Arguments option field:

`-DSWIG_EXECUTABLE=C:\\PATH\\TO\\swigwin-4.0.2\\swig.exe -DPYTHON_EXECUTABLE=C:\\PATH\\TO\\vcpkg\\installed\\x64-windows\\tools\\python3\\python.exe`

Replace the `PATH\\TO\\` section with the appropriate paths to Python and SWIG on your
installation.  Make sure to change single backslashes to double backslashes if you are pasting in
a path from Windows Explorer.

Save the changes. CMake configuration should now succeed, showing "CMake generation finished" in the output window at the bottom.  You can now build! (Build menu > Build all)

==== 5. Running and debugging

Running or debugging KiCad directly from the build directory through Visual Studio requires that
you set certain environment variables so that KiCad can find runtime dependencies and Python works.
To configure the runtime environment for programs run from Visual Studio, edit the `launch.vs.json`
file in the project root directory.  To open this file, select Debug and Launch Settings for kicad
from the Debug menu.

NOTE: If you have multiple launch configurations, for example to debug standalone pcbnew or
      eeschema in addition to kicad, you will need to configure each one separately.

The variable `KICAD_RUN_FROM_BUILD_DIR` must be present (it can be set to any value) to run from
the build directory. Setting this variable allows KiCad to find dynamic libraries when running from
the build directory.

The variable `KICAD_USE_EXTERNAL_PYTHONHOME` must be present (it can be set to any value) when
running from either the build or the install directory, which allows you to specify `PYTHONHOME` in
the environment.  Normally, KiCad on Windows uses a Python that is bundled and placed in a specific
location by the installer, and when running from Visual Studio, Python will be in a different
location.

The variable `PYTHONHOME` must be present and set to the location of the Python installed by vcpkg.

The variable `PYTHONPATH` must be present and set to include the paths to the `pcbnew` python
module and the `kicad_pyshell` module.  The former is built next to `pcbnew.exe` in the build
directory, and the latter lives in the source tree in the `scripting` directory.

Use the `env` section of each configuration entry to set the required environment variables. For
example, the below configuration allows running and debugging `kicad.exe` from the build directory,
assuming `vcpkg` is installed at `C:\code\vcpkg`.

Additionally, use the `environment` section to add the path to the `vcpkg` installed packages
directory, which will allow KiCad to find dynamic libraries from dependencies.

This snippet shows an example launch configuration for running x64-Debug `kicad.exe` from the build
directory.  To run from the install directory, the configuration will look the same except the
`KICAD_RUN_FROM_BUILD_DIR` variable should not be set.

[source,json]
```
{
    "type": "default",
    "project": "CMakeLists.txt",
    "projectTarget": "kicad.exe (kicad\\kicad.exe)",
    "name": "kicad.exe (kicad\\kicad.exe)",
    "env": {
        "KICAD_RUN_FROM_BUILD_DIR": "1",
        "KICAD_USE_EXTERNAL_PYTHONHOME": "1",
        "PYTHONHOME": "C:\\code\\vcpkg\\installed\\x64-windows\\tools\\python3",
        "PYTHONPATH": "C:\\code\\kicad\\build\\x64-Debug\\pcbnew;C:\\code\\kicad\\scripting"
    },
    "environment": [
        {
            "name": "Path",
            "value": "${env.Path};C:\\code\\vcpkg\\installed\\x64-windows\\debug\\bin"
        }
    ]
}
```

Modify the value of `PYTHONPATH`, `PYTHONHOME`, and `Path` depending on where `vcpkg` is installed
and where your `kicad` source and build directories are located.

=== Visual Studio Extensions

==== Trailing Whitespace Remover
It is *highly recommended* users install the link:https://marketplace.visualstudio.com/items?itemName=MadsKristensen.TrailingWhitespaceVisualizer[Trailing Whitespace Visualizer] which will not only highlight trailing whitespace as you type but also automatically remove it by default when you save the file.

=== Troubleshooting

==== Could NOT find GLEW (missing: GLEW_INCLUDE_DIR GLEW_LIBRARY)

You might be trying to perform a 32-bit build (x86-*) despite only having a 64-bit GLEW installed by vcpkg. Switch your configuration to x64-Debug or x64-Release.
